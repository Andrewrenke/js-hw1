// Відповіді на запитання:

// 1. Змінну можна оголосити через "const"(незмінне значення), "let"(змінне значення) або "var"(застаріле значення);
// 2. prompt - дозволяє використовувати поле введення,
//    confirm - запитує у користувача підтвердження певної дії та дає два варіанти відповіді - так, або ні(true or false);
// 3. Неявне перетворення типів відбувається за допомогою надання одному типу різних контейнерів, наприклад:
// let name = 1
// const surname = 6
// name = surname * 3


// // // Excercise 1
// let name = "Andrew"
// let admin = name
// console.log(admin)
//
// // // Excercise 2
// let days = 8
// const formula = 86400
// const seconds = days * formula
// console.log(seconds)

// // Excercise 3
// const userEnter = prompt("Write something here")
// console.log(userEnter)





